﻿using System;
using System.Net;

namespace ltrModulesNet
{
    class NetAddrConverter
    {
        public static uint ipAddrToUint(IPAddress addr)
        {
            byte[] bytes = addr.GetAddressBytes();
            Array.Reverse(bytes); // flip big-endian(network order) to little-endian
            return BitConverter.ToUInt32(bytes, 0);
        }

        public static IPAddress uintToIpAddr(uint intAddr)
        {
            byte[] bytes = BitConverter.GetBytes(intAddr);
            Array.Reverse(bytes);
            return new IPAddress(bytes);
        }
    }
}
