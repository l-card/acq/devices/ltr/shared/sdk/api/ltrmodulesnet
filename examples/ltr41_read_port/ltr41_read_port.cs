﻿using System;
using ltrModulesNet;


/* Данный пример демонстрирует работу с модулем LTR41 из программы на языке C#.
 * Пример представляет собой консольную программу, которая устанавливает связь с модулем,
 * выводит информацию о модуле и читает значения со входов.
 * 
 * Необходимо установить номер слота, в котором вставлен модуль (константа SLOT). 
 */


namespace ltr41_read_port
{
    class ltr41_read_port
    {
         /* Номер слота в крейте, где вставлен модуль */
        const int SLOT = 1;

        static int Main(string[] args)
        {          
            /* LTR41_Init() вызывается уже в конструкторе */
            ltr41api hltr41 = new ltr41api();
            /* отрываем модуль. Используем упрощенный вариант функции с указанием только слота.
             * (есть вариант как с только со слотом, так и с серийным крейта и слотом 
             *  + полный) */
            _LTRNative.LTRERROR err = hltr41.Open(SLOT);
            if (err != _LTRNative.LTRERROR.OK)
            {
                Console.WriteLine("Не удалось открыть модуль. Ошибка {0}: {1}",
                    err, ltr41api.GetErrorString(err));
            }
            else
            {
                /* выводим информацию из hltr42.ModuleInfo */
                Console.WriteLine("Информация о модуле: ");
                Console.WriteLine("  Название модуля: {0}", hltr41.ModuleInfo.Name);
                Console.WriteLine("  Серийный номер : {0}", hltr41.ModuleInfo.Serial);
                Console.WriteLine("  Версия прошивки: {0}", hltr41.ModuleInfo.FirmwareVersionStr);
                Console.WriteLine("  Дата прошивки  : {0}", hltr41.ModuleInfo.FirmwareDateStr);

                /* --------------- конфигурация модуля ------------ */
                err = hltr41.Config();
                if (err != _LTRNative.LTRERROR.OK)
                {
                    Console.WriteLine("Не удалось установить настройки модуля. Ошибка {0}: {1}",
                            err, ltr41api.GetErrorString(err));                    
                }

                if (err == _LTRNative.LTRERROR.OK)
                {
                    /* чтение значений входов */
                    ushort wrd;
                    err = hltr41.ReadPort(out wrd);
                    if (err != _LTRNative.LTRERROR.OK)
                    {
                        Console.WriteLine("Не удалось прочитать значения портов модуля. Ошибка {0}: {1}",
                                err, ltr41api.GetErrorString(err));
                    }
                    else
                    {
                        /* Каждому из входов IN1-IN16 соответствует свой бит в 16-битном
                         * прочитанном слове,  соответствующий считанному значению входа.
                         * Проходимося по битам и выводим на консоль значения каждого входа
                         * с IN16 до IN1 */                     
                        ushort msk;
                        Console.Write("Успешно считано значение: ");
                        for (msk = (1 << 15); msk != 0; msk >>= 1)
                            Console.Write("{0} ", (wrd & msk) != 0 ? 1 : 0);
                        Console.WriteLine("");
                    }              
                }
            }

            /* закрываем соединение */
            if (hltr41.IsOpened() == _LTRNative.LTRERROR.OK)
                hltr41.Close();

            return (int)err;
        }
    }
}
