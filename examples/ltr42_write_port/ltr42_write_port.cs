﻿using System;
using ltrModulesNet;


/* Данный пример демонстрирует работу с модулем LTR42 из программы на языке C#.
 * Пример представляет собой консольную программу, которая устанавливает связь с модулем,
 * выводит информацию о модуле и выводит тестовое значение на выходы.
 * 
 * Необходимо установить номер слота, в котором вставлен модуль (константа SLOT). 
 */


namespace ltr42_write_port
{
    class ltr42_write_port
    {
         /* Номер слота в крейте, где вставлен модуль */
        const int SLOT = 1;

        static int Main(string[] args)
        {          
            /* LTR42_Init() вызывается уже в конструкторе */
            ltr42api hltr42 = new ltr42api();
            /* отрываем модуль. Используем упрощенный вариант функции с указанием только слота.
             * (есть вариант как с только со слотом, так и с серийным крейта и слотом 
             *  + полный) */
            _LTRNative.LTRERROR err = hltr42.Open(SLOT);
            if (err != _LTRNative.LTRERROR.OK)
            {
                Console.WriteLine("Не удалось открыть модуль. Ошибка {0}: {1}",
                    err, ltr42api.GetErrorString(err));
            }
            else
            {
                /* выводим информацию из hltr42.ModuleInfo */
                Console.WriteLine("Информация о модуле: ");
                Console.WriteLine("  Название модуля: {0}", hltr42.ModuleInfo.Name);
                Console.WriteLine("  Серийный номер : {0}", hltr42.ModuleInfo.Serial);
                Console.WriteLine("  Версия прошивки: {0}", hltr42.ModuleInfo.FirmwareVersionStr);
                Console.WriteLine("  Дата прошивки  : {0}", hltr42.ModuleInfo.FirmwareDateStr);

                /* --------------- конфигурация модуля ------------ */
                err = hltr42.Config();
                if (err != _LTRNative.LTRERROR.OK)
                {
                    Console.WriteLine("Не удалось установить настройки модуля. Ошибка {0}: {1}",
                            err, ltr42api.GetErrorString(err));                    
                }

                if (err == _LTRNative.LTRERROR.OK)
                {
                    /* Каждому из выходов OUT1-OUT16 соответствует свой бит в 16-битном
                     * слове, значение которого определяет выводимое значение на соответствующем
                     * выходе. 
                     * В примере выводим "1" значение на OUT1 (бит 0) и OUT10 (бит 9) 
                     * и нулевые значения на все остальные выходы. */
                    ushort wrd = (1 << 0) | (1 << 9);
                    err = hltr42.WritePort(wrd);
                    if (err != _LTRNative.LTRERROR.OK)
                    {
                        Console.WriteLine("Не удалось записать значения портов модуля. Ошибка {0}: {1}",
                                err, ltr42api.GetErrorString(err));
                    }
                    else
                    {
                        Console.WriteLine("Значения портов записаны успешно!");
                    }
                }
            }

            /* закрываем соединение */
            if (hltr42.IsOpened() == _LTRNative.LTRERROR.OK)
                hltr42.Close();

            return (int)err;
        }
    }
}
